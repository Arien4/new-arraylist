package com.epam.stage2.small;

import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Predicate;

public class NewArrayList<T> extends ArrayList<T> {
    public NewArrayList() {
    }

    public NewArrayList(Collection<? extends T> c) {
        super(c);
    }

    public NewArrayList(int initialCapacity) {
        super(initialCapacity);
    }

    @Override
    public T remove(int index) {
        return null;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    protected void removeRange(int fromIndex, int toIndex) {

    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean removeIf(Predicate<? super T> filter) {
        return false;
    }
}
