package com.epam.stage2.small;

import java.util.ArrayList;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        ArrayList<Integer> al = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5));
        NewArrayList<Integer> nal = new NewArrayList<>(Arrays.asList(6, 7, 8, 9, 10));

        System.out.println(al);
        System.out.println(nal);
        al.remove((Integer)4);
        System.out.println(al);
        nal.remove((Integer)9);
        nal.remove(1);
        nal.removeAll(Arrays.asList(6, 7));
        nal.clear();
        System.out.println(nal);

    }
}
